# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.1.1] - 2020-05-17
### Changed
* The module was changed


## [0.1.0] - 2021-05-17
### Added
* The module was added.


[0.1.1]: https://bitbucket.org/vismadc/bekkenstrom-infoeasy-prices-m2/branches/compare/0.1.1%0D0.1.0
[0.1.0]: https://bitbucket.org/vismadc/bekkenstrom-infoeasy-prices-m2/src/0.1.0/
